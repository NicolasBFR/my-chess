/**
 * @author Nicolas
 * @date Start 25/07/2021
 * @details Pawn class file
 */

#include "pawn.h"

#include <vector>

#include "piece.h"

pawn::pawn(int x_ini, int y_ini, bool white, piece *king,
           vector<piece *> *enemy)
    : piece(x_ini, y_ini, enemy, king)
{
    isWhite = white;
    p_type = piece_type::Pawn;
    if (white)
    {
        ch = u8"\u2659";
    }
    else
    {
        ch = u8"\u265F";
    }
}

vector<int> pawn::move(piece *board[8][8])
{
    int dir;
    if (isWhite)
        dir = -1;
    else
        dir = 1;
    vector<int> vect = vector<int>();
    if (y + dir >= 0 && y + dir < 8)
    {
        if (board[x][y + dir] == nullptr)
        {
            vect.push_back(x);
            vect.push_back(y + dir);
        }
        if (!hasMoved && board[x][y + dir] == nullptr
            && board[x][y + dir * 2] == nullptr)
        {
            vect.push_back(x);
            vect.push_back(y + dir * 2);
        }
        if (x + 1 < 8
            && ((board[x + 1][y + dir] != nullptr
                 && board[x + 1][y + dir]->getColor() != isWhite)
                || (y > 2 && y < 5 && board[x + 1][y] != nullptr
                    && !board[x + 1][y]->getMoved())))
        {
            vect.push_back(x + 1);
            vect.push_back(y + dir);
        }
        if (x - 1 >= 0
            && ((board[x - 1][y + dir] != nullptr
                 && board[x - 1][y + dir]->getColor() != isWhite)
                || (y > 2 && y < 5 && board[x - 1][y] != nullptr
                    && !board[x - 1][y]->getMoved())))
        {
            vect.push_back(x - 1);
            vect.push_back(y + dir);
        }
    }
    return vect;
}
piece *pawn::specialMoves(piece *(*board)[8], int i, int j)
{
    if (j == 0 || j == 7)
        return this;
    int dir = j + isWhite - (!isWhite);
    if (j - y > 1 || y - j > 1)
        hasMoved = false;
    else if (board[i][dir] != nullptr
             && board[i][dir]->getType() == piece_type::Pawn
             && !board[i][dir]->getMoved())
        return board[i][dir];
    return nullptr;
}
