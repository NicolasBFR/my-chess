/**
 * @author Nicolas
 * @date Start 25/07/2021
 * @details Rook class file
 */

#include "rook.h"

//#include <string>
#include <vector>

#include "piece.h"

rook::rook(int x_ini, int y_ini, bool white, piece *king,
           vector<piece *> *enemy)
    : piece(x_ini, y_ini, enemy, king)
{
    isWhite = white;
    p_type = piece_type::Rook;
    if (white)
    {
        ch = u8"\u2656";
    }
    else
    {
        ch = u8"\u265C";
    }
}

vector<int> rook::move(piece *board[8][8])
{
    vector<int> vect = vector<int>();
    int i = x - 1;
    for (; i >= 0 && board[i][y] == nullptr; i--)
    {
        vect.push_back(i);
        vect.push_back(y);
    }
    if (i >= 0 && board[i][y]->getColor() != isWhite)
    {
        vect.push_back(i);
        vect.push_back(y);
    }

    i = x + 1;
    for (; i < 8 && board[i][y] == nullptr; i++)
    {
        vect.push_back(i);
        vect.push_back(y);
    }
    if (i < 8 && board[i][y]->getColor() != isWhite)
    {
        vect.push_back(i);
        vect.push_back(y);
    }

    int j = y - 1;
    for (; j >= 0 && board[x][j] == nullptr; j--)
    {
        vect.push_back(x);
        vect.push_back(j);
    }
    if (j >= 0 && board[x][j]->getColor() != isWhite)
    {
        vect.push_back(x);
        vect.push_back(j);
    }

    j = y + 1;
    for (; j < 8 && board[x][j] == nullptr; j++)
    {
        vect.push_back(x);
        vect.push_back(j);
    }
    if (j < 8 && board[x][j]->getColor() != isWhite)
    {
        vect.push_back(x);
        vect.push_back(j);
    }

    return vect;
}
