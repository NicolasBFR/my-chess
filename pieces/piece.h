/**
 * @author Nicolas
 * @date Start 25/07/2021
 * @details Header of piece class file
 */

#ifndef MY_SUMMER_CHESS_PIECE_H
#define MY_SUMMER_CHESS_PIECE_H

#include <string>
#include <vector>

using namespace std;

class piece
{
public:
    enum class piece_type
    {
        King,
        Queen,
        Pawn,
        Rook,
        Bishop,
        Knight,
        None
    };

protected:
    int x;
    int y;
    string ch;
    bool isWhite;
    bool hasMoved;
    piece_type p_type;
    piece *kg;
    vector<piece *> *enemies;

public:
    /**
     * @author Nicolas
     * @date Start 26/04/2021
     * @param x_ini initial horizontal coordinate
     * @param y_ini initial vertical coordinate
     * @param white is the piece white or not
     * @param king the piece king
     * @param enemy the vector of enemy piece on the board
     */
    explicit piece(int x_ini, int y_ini, vector<piece *> *enemy, piece *king);
    //!<  piece constructor

    /**
     * @author Nicolas
     * @date Start 26/04/2021
     */
    ~piece();
    //!<  piece destructor

    /**
     * @author Nicolas
     * @date Start 26/04/2021
     * @param board 2D array with piece at their coordinates
     * @return vector of pair of coordinates that are valid moves
     */
    virtual vector<int> move(piece *board[8][8]);
    //!<  Gives possible moves

    /**
     * @author Nicolas
     * @date Start 26/04/2021
     * @param i horizontal coordinate
     * @param j vertical coordinate
     */
    void SetCoor(int i, int j);
    //!<  Setter of coordinates

    /**
     * @author Nicolas
     * @date Start 26/04/2021
     * @return isWhite
     */
    bool getColor() const;
    //!<  Getter of the color

    /**
     * @author Nicolas
     * @date Start 26/04/2021
     * @return ch
     */
    string getChar();
    //!<  Getter of the piece character

    /**
     * @author Nicolas
     * @date Start 26/04/2021
     * @return x
     */
    int getX() const;
    //!<  Getter of the piece horizontal coordinate

    /**
     * @author Nicolas
     * @date Start 26/04/2021
     * @return y
     */
    int getY() const;
    //!<  Getter of the piece vertical coordinate

    /**
     * @author Nicolas
     * @date Start 26/04/2021
     * @return hasMoved
     */
    bool getMoved() const;
    //!<  Return true if the piece has moved

    /**
     * @author Nicolas
     * @date Start 26/04/2021
     */
    void setMoved();
    //!<  Set a piece as "has moved"

    /**
     * @author Nicolas
     * @date Start 26/04/2021
     * @return the piece type (by enumeration)
     */
    piece_type getType();
    //!<  returns the piece type

    /**
     * @author Nicolas
     * @date Start 26/04/2021
     * @param board 2D array with piece at their coordinates
     * @param i destination horizontal coordinate
     * @param j destination vertical coordinate
     * @return true if the move put the king in check, false otherwise
     */
    virtual bool isKgSafe(piece *board[8][8], int i, int j);
    //!<  determine if a piece possible move is safe for the king

    /**
     * @author Nicolas
     * @date Start 26/04/2021
     * @param board 2D array with piece at their coordinates
     * @param moved the piece to be moved
     * @param i destination horizontal coordinate
     * @param j destination vertical coordinate
     * @return the piece that has been overwriten in the destination case
     */
    piece *tmpMovePiece(piece *board[8][8], piece *moved, int i, int j);
    //!<  moves a piece without displaying it and telling the rest of the board

    /**
     * @author Nicolas
     * @date Start 26/04/2021
     * @param board 2D array with piece at their coordinates
     * @return vector of pair of coordinates that are valid, safe moves
     */
    vector<int> safeMoves(piece *board[8][8]);
    //!<  returns all safe moves for the selected piece

    /**
     * @author Nicolas
     * @date Start 26/04/2021
     * @param board 2D array with piece at their coordinates
     * @param i destination horizontal coordinate
     * @param j destination vertical coordinate
     * @return nullptd
     */
    virtual piece *specialMoves(piece *board[8][8], int i, int j);
    //!<  meant to be override by pieces to declare pieces affected by a special
    //!<  move
};

#endif // MY_SUMMER_CHESS_PIECE_H
