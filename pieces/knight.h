/**
 * @author Nicolas
 * @date Start 25/07/2021
 * @details Header of knight class file
 */

#ifndef MY_SUMMER_CHESS_KNIGHT_H
#define MY_SUMMER_CHESS_KNIGHT_H
#include <string>
#include <vector>

#include "piece.h"

class knight : public piece
{
public:
    /**
     * @author Nicolas
     * @date Start 26/04/2021
     * @param x_ini initial horizontal coordinate
     * @param y_ini initial vertical coordinate
     * @param white is the piece white or not
     * @param king the piece king
     * @param enemy the vector of enemy piece on the board
     */
    knight(int x_ini, int y_ini, bool white, piece *king,
           vector<piece *> *enemy);
    //!<  Knight constructor

    /**
     * @author Nicolas
     * @date Start 26/04/2021
     * @param board 2D array with piece at their coordinates
     * @return vector of pair of coordinates that are valid moves
     */
    vector<int> move(piece *board[8][8]) override;
    //!<  Gives possible moves
};

#endif // MY_SUMMER_CHESS_KNIGHT_H
