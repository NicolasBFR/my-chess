/**
 * @author Nicolas
 * @date Start 25/07/2021
 * @details Knight class file
 */

#include "knight.h"

//#include <string>
#include <vector>

#include "piece.h"

knight::knight(int x_ini, int y_ini, bool white, piece *king,
               vector<piece *> *enemy)
    : piece(x_ini, y_ini, enemy, king)
{
    isWhite = white;
    p_type = piece_type::Knight;
    if (white)
    {
        ch = u8"\u2658";
    }
    else
    {
        ch = u8"\u265E";
    }
}

vector<int> knight::move(piece *board[8][8])
{
    vector<int> vect = vector<int>();
    for (int i = -2; i < 3; i++)
    {
        for (int j = -2; j < 3; j++)
        {
            if (i != 0 && j != 0 && i != j && i != -j && i + x >= 0
                && j + y >= 0 && i + x < 8 && j + y < 8
                && (board[i + x][j + y] == nullptr
                    || isWhite != board[i + x][j + y]->getColor()))
            {
                vect.push_back(i + x);
                vect.push_back(j + y);
            }
        }
    }
    return vect;
}
