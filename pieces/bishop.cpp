/**
 * @author Nicolas
 * @date Start 25/07/2021
 * @details Bishop class file
 */

#include "bishop.h"

#include <vector>

#include "piece.h"
//#include <iostream>
//#include <string>

bishop::bishop(int x_ini, int y_ini, bool white, piece *king,
               vector<piece *> *enemy)
    : piece(x_ini, y_ini, enemy, king)
{
    isWhite = white;
    p_type = piece_type::Bishop;
    if (white)
    {
        ch = u8"\u2657";
    }
    else
    {
        ch = u8"\u265D";
    }
}

vector<int> bishop::move(piece *board[8][8])
{
    vector<int> vect = vector<int>();
    int i = x - 1, j = y - 1;
    for (; i >= 0 && j >= 0 && board[i][j] == nullptr; i--, j--)
    {
        vect.push_back(i);
        vect.push_back(j);
    }
    if (i >= 0 && j >= 0 && board[i][j]->getColor() != isWhite)
    {
        vect.push_back(i);
        vect.push_back(j);
    }

    i = x - 1, j = y + 1;
    for (; i >= 0 && j < 8 && board[i][j] == nullptr; i--, j++)
    {
        vect.push_back(i);
        vect.push_back(j);
    }
    if (i >= 0 && j < 8 && board[i][j]->getColor() != isWhite)
    {
        vect.push_back(i);
        vect.push_back(j);
    }

    i = x + 1, j = y - 1;
    for (; i < 8 && j >= 0 && board[i][j] == nullptr; i++, j--)
    {
        vect.push_back(i);
        vect.push_back(j);
    }
    if (i < 8 && j >= 0 && board[i][j]->getColor() != isWhite)
    {
        vect.push_back(i);
        vect.push_back(j);
    }

    i = x + 1, j = y + 1;
    for (; i < 8 && j < 8 && board[i][j] == nullptr; i++, j++)
    {
        vect.push_back(i);
        vect.push_back(j);
    }
    if (i < 8 && j < 8 && board[i][j]->getColor() != isWhite)
    {
        vect.push_back(i);
        vect.push_back(j);
    }

    return vect;
}
