/**
 * @author Nicolas
 * @date Start 25/07/2021
 * @details Header to include all pieces
 */

#ifndef MY_SUMMER_CHESS_INCLUDE_H
#define MY_SUMMER_CHESS_INCLUDE_H

#include "bishop.h"
#include "king.h"
#include "knight.h"
#include "pawn.h"
#include "piece.h"
#include "queen.h"
#include "rook.h"

#endif // MY_SUMMER_CHESS_INCLUDE_H
