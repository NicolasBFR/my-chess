/**
 * @author Nicolas
 * @date Start 25/07/2021
 * @details King class file
 */

#include "king.h"

//#include <string>
#include <vector>

#include "piece.h"

king::king(int x_ini, int y_ini, bool white, vector<piece *> *enemy)
    : piece(x_ini, y_ini, enemy, this)
{
    isWhite = white;
    p_type = piece_type::King;
    if (white)
    {
        ch = u8"\u2654";
    }
    else
    {
        ch = u8"\u265A";
    }
}

vector<int> king::move(piece *board[8][8])
{
    vector<int> vect = vector<int>();
    for (int i = -1; i < 2; i++)
    {
        for (int j = -1; j < 2; j++)
        {
            if (i != 0 || j != 0)
            {
                int tmpi = x + i, tmpj = y + j;
                if (tmpi >= 0 && tmpi < 8 && tmpj >= 0 && tmpj < 8
                    && (board[tmpi][tmpj] == nullptr
                        || board[tmpi][tmpj]->getColor() != isWhite))
                {
                    vect.push_back(tmpi);
                    vect.push_back(tmpj);
                }
            }
        }
    }
    if (!hasMoved && board[5][y] == nullptr && board[6][y] == nullptr
        && board[7][y] != nullptr && !board[7][y]->getMoved())
    {
        vect.push_back(6);
        vect.push_back(y);
    }
    if (!hasMoved && board[3][y] == nullptr && board[2][y] == nullptr
        && board[1][y] == nullptr && board[0][y] != nullptr
        && !board[0][y]->getMoved())
    {
        vect.push_back(2);
        vect.push_back(y);
    }
    return vect;
}

bool king::isKgSafe(piece *board[8][8], int i, int j)
{
    if ((x + 2 == i && !isKgSafe(board, i - 1, j))
        || (x - 2 == i && !isKgSafe(board, i + 1, j)))
        return false;
    int tmpX = x, tmpY = y;
    piece *tmp = tmpMovePiece(board, this, i, j);
    for (piece *k : *enemies)
    {
        if (k != tmp)
        {
            vector<int> pos = k->move(board);
            while (!pos.empty())
            {
                int tmpi = pos[0];
                pos.erase(pos.begin());
                int tmpj = pos[0];
                pos.erase(pos.begin());
                if (tmpi == x && tmpj == y)
                {
                    tmpMovePiece(board, this, tmpX, tmpY);
                    tmpMovePiece(board, tmp, i, j);
                    return false;
                }
            }
        }
    }
    tmpMovePiece(board, this, tmpX, tmpY);
    tmpMovePiece(board, tmp, i, j);
    return true;
}
piece *king::specialMoves(piece *(*board)[8], int i, int j)
{
    if (x + 2 == i)
        return board[7][y];
    else if (x - 2 == i)
        return board[0][y];
    return nullptr;
}
