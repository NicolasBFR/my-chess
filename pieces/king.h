/**
 * @author Nicolas
 * @date Start 25/07/2021
 * @details Header of king class file
 */

#ifndef MY_SUMMER_CHESS_KING_H
#define MY_SUMMER_CHESS_KING_H
#include <string>
#include <vector>

#include "piece.h"

class king : public piece
{
public:
    /**
     * @author Nicolas
     * @date Start 26/04/2021
     * @param x_ini initial horizontal coordinate
     * @param y_ini initial vertical coordinate
     * @param white is the piece white or not
     * @param king the piece king
     * @note obvisouly here, king is this
     * @param enemy the vector of enemy  piece on the board
     */
    king(int x_ini, int y_ini, bool white, vector<piece *> *enemy);
    //!<  King constructor

    /**
     * @author Nicolas
     * @date Start 26/04/2021
     * @param board 2D array with piece at their coordinates
     * @return vector of pair of coordinates that are valid moves
     */
    vector<int> move(piece *board[8][8]) override;
    //!<  Gives possible moves

    /**
     * @author Nicolas
     * @date Start 26/04/2021
     * @param board 2D array with piece at their coordinates
     * @param i destination horizontal coordinate
     * @param j destination vertical coordinate
     * @return true if the move put the king in check, false otherwise
     */
    bool isKgSafe(piece *board[8][8], int i, int j) override;
    //!<  determine if a piece possible move is safe for the king

    /**
     * @author Nicolas
     * @date Start 26/04/2021
     * @param board 2D array with piece at their coordinates
     * @param i destination horizontal coordinate
     * @param j destination vertical coordinate
     * @return rook to move due to castling
     */
    piece *specialMoves(piece *board[8][8], int i, int j) override;
    //!<  returns rook to move due to castling
};

#endif // MY_SUMMER_CHESS_KING_H
