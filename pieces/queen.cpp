/**
 * @author Nicolas
 * @date Start 25/07/2021
 * @details Queen class file
 */

#include "queen.h"

//#include <string>
#include <vector>
#include <iterator>

#include "bishop.h"
#include "piece.h"
#include "rook.h"

queen::queen(int x_ini, int y_ini, bool white, piece *king,
             vector<piece *> *enemy)
    : piece(x_ini, y_ini, enemy, king)
{
    isWhite = white;
    p_type = piece_type::Queen;
    if (white)
    {
        ch = u8"\u2655";
    }
    else
    {
        ch = u8"\u265B";
    }
}

vector<int> queen::move(piece *board[8][8])
{
    bishop tmp1(x, y, isWhite, kg, enemies);
    rook tmp2(x, y, isWhite, kg, enemies);
    vector<int> vect1 = tmp1.move(board);
    vector<int> vect2 = tmp2.move(board);
    std::move(vect2.begin(), vect2.end(), std::back_inserter(vect1));
    return vect1;
}
