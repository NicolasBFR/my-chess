/**
 * @author Nicolas
 * @date Start 25/07/2021
 * @details Piece class file
 */

#include "piece.h"

#include <string>
#include <vector>

using namespace std;

piece::piece(int x_ini, int y_ini, vector<piece *> *enemy,
             piece *king = nullptr)
{
    isWhite = true;
    x = x_ini, y = y_ini;
    ch = "";
    hasMoved = false;
    p_type = piece_type::None;
    kg = king;
    enemies = enemy;
}

piece::~piece() = default;

vector<int> piece::move(piece *board[8][8])
{
    return vector<int>();
}

vector<int> piece::safeMoves(piece *board[8][8])
{
    vector<int> pos = move(board);
    for (int i = 0; i < pos.size(); i += 2)
    {
        if (!isKgSafe(board, pos[i], pos[i + 1]))
        {
            pos.erase(pos.begin() + i);
            pos.erase(pos.begin() + i);
            i -= 2;
        }
    }
    return pos;
}

bool piece::isKgSafe(piece *board[8][8], int i, int j)
{
    int tmpX = x, tmpY = y;
    piece *tmp = tmpMovePiece(board, this, i, j);
    for (piece *k : *enemies)
    {
        if (k != tmp)
        {
            vector<int> pos = k->move(board);
            while (!pos.empty())
            {
                int tmpi = pos[0];
                pos.erase(pos.begin());
                int tmpj = pos[0];
                pos.erase(pos.begin());
                if (tmpi == kg->getX() && tmpj == kg->getY())
                {
                    tmpMovePiece(board, this, tmpX, tmpY);
                    tmpMovePiece(board, tmp, i, j);
                    return false;
                }
            }
        }
    }
    tmpMovePiece(board, this, tmpX, tmpY);
    tmpMovePiece(board, tmp, i, j);
    return true;
}

piece *piece::tmpMovePiece(piece *board[8][8], piece *moved, int i, int j)
{
    if (moved == nullptr)
        return nullptr;
    board[moved->getX()][moved->getY()] = nullptr;
    piece *tmp = board[i][j];
    board[i][j] = moved;
    moved->SetCoor(i, j);
    return tmp;
}

bool piece::getColor() const
{
    return isWhite;
}

string piece::getChar()
{
    return ch;
}

void piece::SetCoor(int i, int j)
{
    x = i;
    y = j;
}

int piece::getX() const
{
    return x;
}

int piece::getY() const
{
    return y;
}

bool piece::getMoved() const
{
    return hasMoved;
}

void piece::setMoved()
{
    hasMoved = true;
}
piece::piece_type piece::getType()
{
    return p_type;
}
piece *piece::specialMoves(piece *(*board)[8], int i, int j)
{
    return nullptr;
}
