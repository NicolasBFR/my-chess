/**
 * @author Nicolas
 * @date Start 25/07/2021
 * @details Header of game logic file
 */

#ifndef MY_SUMMER_CHESS_BOARD_H
#define MY_SUMMER_CHESS_BOARD_H

#include "../interface/chessUI.h"
#include "../pieces/include.h"

class chessUI;

/**
 * @author Nicolas
 * @date Start 26/04/2021
 * @details Class of the game board
 * @note Everywhere in the code, true means white, false black
 */
class board
{
private:
    piece *chessBoard[8][8];
    bool blBoard[8][8];
    piece *selected;
    bool turn;
    vector<piece *> whitePieces;
    vector<piece *> blackPieces;
    chessUI *UI;

public:
    /**
     * @author Nicolas
     * @date Start 26/04/2021
     */
    explicit board(); //!< Constructor of game board

    /**
     * @author Nicolas
     * @date Start 26/04/2021
     * @details
     * @param i horizontal case coordinate
     * @param j vectical case coordinate
     */
    void caseClicked(int i, int j);
    //!< Handles User interaction: either gives possible moves or move the piece

    /**
     * @author Nicolas
     * @date Start 26/04/2021
     */
    void clearDownCases(); //!< Remove all indication of possible moves

    /**
     * @author Nicolas
     * @date Start 26/04/2021
     * @param i horizontal piece coordinate
     * @param j vectical piece coordinate
     */
    void removePiece(int i, int j);
    //!< Remove a piece from the piece vector and off the board

    /**
     * @author Nicolas
     * @date Start 26/04/2021
     * @param white whether you want white or black pieces
     */
    vector<piece *> *getPieces(bool white);
    //!< Gives a team piece list

    /**
     * @author Nicolas
     * @date Start 26/04/2021
     */
    void checkmate();
    //!< Check if the player can make a move, stop the game if not

    /**
     * @author Nicolas
     * @date Start 26/04/2021
     * @param i horizontal destination case coordinate
     * @param j vectical destination case coordinate
     */
    void movePiece(piece *moved, int i, int j);
    //!< Move a piece on the board

    /**
     * @author Nicolas
     * @date Start 26/04/2021
     * @param promoted The pawn to promote
     */
    void pawnPromotion(piece *promoted);
    //!< Promote a pawn into desired piece

    /**
     * @author Nicolas
     * @date Start 26/04/2021
     */
    ~board();
    //!< Board destructor

    /**
     * @author Nicolas
     * @date Start 26/04/2021
     * @note Board and UI needs each other and UI is initialized AFTER board in
     * the main
     * @param given the board to link to the board
     */
    void setUI(chessUI *given);
    //!< Set the board's UI
};

#endif // MY_SUMMER_CHESS_BOARD_H
