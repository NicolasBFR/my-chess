/**
 * @author Nicolas
 * @date Start 25/07/2021
 * @details Game logic file
 */

#include "board.h"

#include <string>
#include <vector>

#include "../interface/chessUI.h"
#include "../pieces/include.h"

board::board()
{
    // Board creation
    for (int a = 0; a < 8; a++)
    {
        for (int b = 0; b < 8; b++)
        {
            chessBoard[a][b] = nullptr;
            blBoard[a][b] = false;
        }
    }

    selected = nullptr;
    turn = true;
    // Initial pieces

    chessBoard[4][0] = new king(4, 0, false, &whitePieces);
    blackPieces.push_back(chessBoard[4][0]);
    chessBoard[4][7] = new king(4, 7, true, &blackPieces);
    whitePieces.push_back(chessBoard[4][7]);

    for (int i = 0; i < 2; i++)
    {
        chessBoard[3][i * 7] =
            new queen(3, i * 7, i, getPieces(i)->at(0), getPieces((!i)));
        int ii = i;
        if (ii < 1)
            ii--;
        for (int j = 0; j < 2; j++)
        {
            chessBoard[i * 7][j * 7] =
                new rook(i * 7, j * 7, j, getPieces(j)->at(0), getPieces((!j)));
            chessBoard[i * 7 - ii][j * 7] = new knight(
                i * 7 - ii, j * 7, j, getPieces(j)->at(0), getPieces((!j)));
            ii *= 2;
            chessBoard[i * 7 - ii][j * 7] = new bishop(
                i * 7 - ii, j * 7, j, getPieces(j)->at(0), getPieces((!j)));
            ii /= 2;
        }
        for (int j = 0; j < 8; j++)
        {
            chessBoard[j][i * 7 - ii] = new pawn(
                j, i * 7 - ii, i, getPieces(i)->at(0), getPieces((!i)));
        }
    }

    // Displaying initial pieces + indexing them
    for (int i = 0; i < 8; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            piece *tmp = chessBoard[j][i];
            if (tmp != nullptr)
            {
                if (i > 0 && i < 7 || j != 4)
                    getPieces(tmp->getColor())->push_back(tmp);
            }
        }
    }
}

void board::clearDownCases()
{
    UI->clearDownCases();
    for (int a = 0; a < 8; a++)
    {
        for (int b = 0; b < 8; b++)
        {
            blBoard[a][b] = false;
        }
    }
}

void board::removePiece(int i, int j)
{
    if (chessBoard[i][j] == nullptr)
        return;
    if (chessBoard[i][j]->getColor())
    {
        for (int k = 0; k < whitePieces.size(); k++)
        {
            if (whitePieces[k] == chessBoard[i][j])
            {
                whitePieces.erase(whitePieces.begin() + k);
            }
        }
    }
    else
    {
        for (int k = 0; k < blackPieces.size(); k++)
        {
            if (blackPieces[k] == chessBoard[i][j])
            {
                blackPieces.erase(blackPieces.begin() + k);
            }
        }
    }
    delete chessBoard[i][j];
    chessBoard[i][j] = nullptr;
}

void board::caseClicked(int i, int j)
{
    if (chessBoard[i][j] != nullptr && chessBoard[i][j]->getColor() == turn)
    {
        clearDownCases();
        selected = chessBoard[i][j];
        vector<int> pos = selected->safeMoves(chessBoard);
        while (!pos.empty())
        {
            int tmpi = pos[0];
            pos.erase(pos.begin());
            int tmpj = pos[0];
            pos.erase(pos.begin());
            UI->setBDown(tmpi, tmpj);
            blBoard[tmpi][tmpj] = true;
        }
    }
    else if (selected != nullptr && blBoard[i][j])
    {
        removePiece(i, j);
        movePiece(selected, i, j);
        selected = nullptr;
        turn = !turn;
        for (piece *k : *getPieces(turn))
        {
            if (k->getY() < 6 && k->getY() > 1)
                k->setMoved();
        }
        clearDownCases();
        checkmate();
    }
}

vector<piece *> *board::getPieces(bool white)
{
    if (white)
        return &whitePieces;
    return &blackPieces;
}

void board::checkmate()
{
    for (piece *ally : *getPieces(turn))
    {
        if (!ally->safeMoves(chessBoard).empty())
            return;
    }

    piece *kg = getPieces(turn)->at(0);
    UI->endgame(kg->isKgSafe(chessBoard, kg->getX(), kg->getY()));
}

void board::movePiece(piece *moved, int i, int j)
{
    UI->setBText("", moved->getX(), moved->getY());
    chessBoard[moved->getX()][moved->getY()] = nullptr;
    UI->setBText(moved->getChar(), i, j);
    chessBoard[i][j] = moved;
    moved->setMoved();
    piece *special = moved->specialMoves(chessBoard, i, j);

    if (special == nullptr)
    {
        moved->SetCoor(i, j);
        return;
    }
    switch (special->getType())
    {
    case piece::piece_type::Pawn: {
        if (special == moved)
        {
            moved->SetCoor(i, j);
            pawnPromotion(special);
        }
        else
        {
            UI->setBText("", special->getX(), special->getY());
            removePiece(special->getX(), special->getY());
        }
        break;
    }
    case piece::piece_type::Rook: {
        if (special->getX() == 7)
            movePiece(special, 5, special->getY());
        else
            movePiece(special, 3, special->getY());
        break;
    }
    default:
        break;
    }
    moved->SetCoor(i, j);
}

board::~board()
{
    for (auto &a : chessBoard)
    {
        for (piece *b : a)
        {
            delete b;
        }
    }
}

void board::pawnPromotion(piece *p)
{
    int i = UI->promotion();
    int tmpX = p->getX(), tmpY = p->getY();
    removePiece(p->getX(), p->getY());
    switch (i)
    {
    case 0:
        chessBoard[tmpX][tmpY] = new knight(
            tmpX, tmpY, turn, getPieces(turn)->at(0), getPieces(!turn));
        break;
    case 1:
        chessBoard[tmpX][tmpY] = new bishop(
            tmpX, tmpY, turn, getPieces(turn)->at(0), getPieces(!turn));
        break;
    case 2:
        chessBoard[tmpX][tmpY] = new rook(
            tmpX, tmpY, turn, getPieces(turn)->at(0), getPieces(!turn));
        break;
    case 3:
        chessBoard[tmpX][tmpY] = new queen(
            tmpX, tmpY, turn, getPieces(turn)->at(0), getPieces(!turn));
        break;
    default:
        chessBoard[tmpX][tmpY] = new queen(
            tmpX, tmpY, turn, getPieces(turn)->at(0), getPieces(!turn));
        break;
    }
    UI->setBText(chessBoard[tmpX][tmpY]->getChar(), tmpX, tmpY);
    getPieces(turn)->push_back(chessBoard[tmpX][tmpY]);
}
void board::setUI(chessUI *given)
{
    UI = given;
    for (int i = 0; i < 8; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            piece *tmp = chessBoard[i][j];
            if (tmp != nullptr)
            {
                UI->setBText(tmp->getChar(), i, j);
            }
        }
    }
}
