/**
 * @author Nicolas
 * @date Start 25/07/2021
 * @details main file
 */

#include <string>

#include "interface/chessUI.h"
#include "pboard/board.h"
#include "QApplication"
#include "QPushButton"
#include "QWidget"

int main(int argc, char **argv)
{
    QApplication app(argc, argv);

    QWidget window;

    board *boardd = new board();
    chessUI *UI = new chessUI(&window, boardd);
    boardd->setUI(UI);
    window.show();
    int i = app.exec();
    delete boardd;
    // delete UI;
    return i;
}
