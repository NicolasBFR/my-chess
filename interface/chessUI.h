/**
 * @author Nicolas
 * @date Start 25/07/2021
 * @details Header of User Interface file
 */

#ifndef MY_SUMMER_CHESS_CHESSUI_H
#define MY_SUMMER_CHESS_CHESSUI_H
#include <string>

#include "../pboard/board.h"
#include "QPushButton"
#include "QWidget"

class board;

class chessUI : public QWidget
{
private:
    QPushButton *qtBoard[8][8];
    board *game;

public:
    /**
     * @author Nicolas
     * @date Start 26/04/2021
     * @param window the program window
     * @param gamee the program game
     */
    explicit chessUI(QWidget *window, board *gamee);
    //!< UI constructor

    /**
     * @author Nicolas
     * @date Start 26/04/2021
     */
    void clearDownCases();
    //!< Remove all indication of possible moves

    /**
     * @author Nicolas
     * @date Start 26/04/2021
     * @param i true means stalemate, false checkmate
     */
    void endgame(bool i);
    //!<  Display checkmate or stalemate messages

    /**
     * @author Nicolas
     * @date Start 26/04/2021
     */
    int promotion();
    //!< Prompt the user to choose a piece to promote the pawn

    /**
     * @author Nicolas
     * @date Start 26/04/2021
     * @param ch the character to set
     * @param i horizontal case coordinate
     * @param j vectical case coordinate
     */
    void setBText(std::string ch, int i, int j);
    //!< Set the character displayed in a case

    /**
     * @author Nicolas
     * @date Start 26/04/2021
     * @param i horizontal case coordinate
     * @param j vectical case coordinate
     */
    void setBDown(int i, int j);
    //!< Set a case as possible move for selected piece
};

#endif // MY_SUMMER_CHESS_CHESSUI_H
