/**
 * @author Nicolas
 * @date Start 25/07/2021
 * @details User Interface file
 */

#include "chessUI.h"

#include <string>

#include "../pboard/board.h"
#include "QApplication"
#include "QMessageBox"
#include "QPushButton"
#include "QWidget"
chessUI::chessUI(QWidget *window, board *gamee)
{
    game = gamee;
    window->setFixedSize(960, 960);
    window->setWindowTitle("My Chess");

    for (int a = 0; a < 8; a++)
    {
        for (int b = 0; b < 8; b++)
        {
            qtBoard[a][b] = new QPushButton("", window);
            qtBoard[a][b]->setGeometry(120 * a, 120 * b, 120, 120);
            if ((b * 8 + a + (b % 2)) % 2 == 1)
                qtBoard[a][b]->setStyleSheet(
                    "QPushButton { border: none; background-color: #779952; "
                    "font-size: 105px;} "
                    "QPushButton:pressed{background-color: #658246;} ;");
            else
                qtBoard[a][b]->setStyleSheet(
                    "QPushButton { border: none; background-color: #edeed1; "
                    "font-size: 105px;} "
                    "QPushButton:pressed{background-color: #dadca0;} ;");
            connect(qtBoard[a][b], &QPushButton::clicked, window,
                    [=] { game->caseClicked(a, b); });
        }
    }

    window->show();
}
void chessUI::clearDownCases()
{
    for (int a = 0; a < 8; a++)
    {
        for (int b = 0; b < 8; b++)
        {
            if (qtBoard[a][b]->isDown())
            {
                qtBoard[a][b]->setDown(false);
            }
        }
    }
}
void chessUI::endgame(bool i)
{
    for (auto &a : qtBoard)
    {
        for (QPushButton *b : a)
        {
            b->disconnect();
        }
    }
    QMessageBox msg;
    msg.setWindowTitle("My Chess");
    if (i)
        msg.setText("          Pat !           ");
    else
        msg.setText("     Echec et mat !       ");
    msg.setStandardButtons(QMessageBox::Discard);
    msg.setDefaultButton(QMessageBox::Discard);
    msg.exec();
}
int chessUI::promotion()
{
    QMessageBox msgBox;
    msgBox.setText(tr("Which piece do you want ?"));
    QAbstractButton *qKnight =
        msgBox.addButton(tr("Knight"), QMessageBox::ActionRole);
    QAbstractButton *qBishop =
        msgBox.addButton(tr("Bishop"), QMessageBox::ActionRole);
    QAbstractButton *qRook =
        msgBox.addButton(tr("Rook"), QMessageBox::ActionRole);
    QAbstractButton *qQueen =
        msgBox.addButton(tr("Queen"), QMessageBox::ActionRole);
    msgBox.exec();
    if (msgBox.clickedButton() == qKnight)
        return 0;
    else if (msgBox.clickedButton() == qBishop)
        return 1;
    else if (msgBox.clickedButton() == qRook)
        return 2;
    else if (msgBox.clickedButton() == qQueen)
        return 3;
    return 3;
}
void chessUI::setBText(string ch, int i, int j)
{
    qtBoard[i][j]->setText(QString::fromStdString(ch));
}
void chessUI::setBDown(int i, int j)
{
    qtBoard[i][j]->setDown(true);
}
